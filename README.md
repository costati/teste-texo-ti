# Produtores com mais premios

A api tem como objetivo listar os produtores com maior intervalo entre dois prêmios, e o que obteve dois prêmios mais rápido.

## Rodar aplicação

Assim que rodar a aplicação a base de dados sera preenchida de forma automatica e a porta que a aplicação esta subindo é 9000.
Os endpoints são responsaveis por manipular os dados das entidade(Movie, Studio e Producer);
Exemplos:
*    http://localhost:9000/api/studios
*    http://localhost:9000/api/producers
*    http://localhost:9000/api/movies

o principal endpoint é o http://localhost:9000/api/producers/winners que retorna o produtor com menor tempo entre dois premios e com maior tempo;
Exemplo de retorno:
-    {
 -       "min": [
            {
                "producer": "Joel Silver",
                "internal": 1,
                "previousWin": 1990,
                "followingWin": 1991
            }
        ],
        "max": [
            {
                "producer": "Matthew Vaughn",
                "internal": 13,
                "previousWin": 2002,
                "followingWin": 2015
            }
        ]
    }


## Rodar os testes

Os testes são efetuados de forma autmatica quando feito o package do projeto, porem pode ser feito de forma unitaria acessando a classe de teste correspondente a entidade.

A ideia dos testes tem como objetivo, garantir as principais funcionalidade da api.


## Tecnologias utilizadas

* Maven
* Junit
* opencsv
* Spring Boot
* JPA
* liquibase


## Versionamento

o projeto esta no githlab: https://gitlab.com/costati/teste-texo-ti 
