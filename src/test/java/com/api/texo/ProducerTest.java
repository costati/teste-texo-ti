package com.api.texo;

import com.api.texo.domain.dto.Winners;
import com.api.texo.domain.entity.Producer;
import com.api.texo.domain.service.MovieService;
import com.api.texo.domain.service.ProducerService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@DependsOn("liquibase")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ProducerTest {

    @Autowired
    ProducerService producerService;

    @Autowired
    MovieService movieService;

    @Test
    public void findProducerById() {
        Optional<Producer> producer = producerService.findById((long) 1);
        Assertions.assertThat(producer)
                .isNotEmpty();
    }

    @Test
    public void findProducerByNameVazio() {
        Producer producer = producerService.findByName("");
        Assertions.assertThat(producer)
                .isNull();
    }

    @Test
    public void create(){

        Producer producer = new Producer();
        producer.setName("Rubas Smith");

        producerService.save(producer);

        Producer producer1 = producerService.findByName(producer.getName());

        Assertions.assertThat(producer1)
                .isNotNull();

        producerService.delete(producer1);
    }

    @Test
    public void update(){

        Producer producer = new Producer();
        producer.setName("Rubas Smith");

        producerService.save(producer);

        Producer producer1 = producerService.findByName(producer.getName());

        producer1.setName("Smith Rubas");

        producer = producerService.save(producer1);

        Assertions.assertThat(producer.getName())
                .isEqualTo(producer1.getName());

        producerService.delete(producer);
    }

    @Test
    public void delete(){
        Producer producer = new Producer();
        producer.setName("John Rufos");

        producer = producerService.save(producer);

        producerService.delete(producer);

        producer = producerService.findByName("Merlin Smith");

        Assertions.assertThat(producer)
                .isNull();

    }

    @Test
    public void winners(){

        Winners winners = producerService.searchPoducerWinners();

        Assertions.assertThat(winners.getMax().size()).isNotZero() ;
        Assertions.assertThat(winners.getMin().size()).isNotZero() ;

    }

    @Test
    public void empty(){

        movieService.deleteAll();

        Winners winners = producerService.searchPoducerWinners();

        Assertions.assertThat(winners.getMax().size()).isEqualTo(0) ;
        Assertions.assertThat(winners.getMin().size()).isEqualTo(0) ;

    }


}


