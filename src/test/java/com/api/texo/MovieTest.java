package com.api.texo;

import com.api.texo.domain.dto.Winners;
import com.api.texo.domain.entity.Movie;
import com.api.texo.domain.service.MovieService;
import com.api.texo.domain.service.ProducerService;
import com.api.texo.domain.service.StudioService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@DependsOn("liquibase")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class MovieTest {

    @Autowired
    MovieService movieService;

    @Test
    public void findMovieById(){
        Optional<Movie> movie = movieService.findById((long)2);

        Assertions.assertThat(movie)
                .isNotEmpty();
    }

    @Test
    public void create(){
        Movie movie = new Movie();
        movie.setTitle("Dragon");
        movie.setYear(2030);
        movie.setWinner(false);

        movie = movieService.save(movie);
        Optional<Movie> movie1 = movieService.findById(movie.getId());

        Assertions.assertThat(movie1)
                .isNotEmpty();

        movieService.deleteById(movie1.get().getId());
    }

    @Test
    public void update(){

        Movie movie = new Movie();
        movie.setTitle("Power");
        movie.setYear(2030);
        movie.setWinner(false);

        movie = movieService.save(movie);
        movie.setTitle("Olavo");
        Movie movie1 = movieService.save(movie);

        Assertions.assertThat(movie1.getTitle())
                .isEqualTo(movie.getTitle());

        movieService.deleteById(movie1.getId());

    }

    @Test
    public void delete(){
        Movie movie = new Movie();
        movie.setTitle("Dark");
        movie.setYear(2030);
        movie.setWinner(false);

        movie = movieService.save(movie);

        movieService.delete(movie);

        Assertions.assertThat(movieService.findById(movie.getId()))
                .isEmpty();

    }

}
