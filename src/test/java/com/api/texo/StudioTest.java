package com.api.texo;

import com.api.texo.domain.entity.Studio;
import com.api.texo.domain.service.StudioService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@DependsOn("liquibase")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class StudioTest {

    @Autowired
    StudioService studioService;

    @Test
    public void findStudioById() {
        Optional<Studio> studio = studioService.findById((long) 1);
        Assertions.assertThat(studio)
                .isNotEmpty();
    }

    @Test
    public void findStudioByNameVazio() {
        Studio studio = studioService.findByName("");
        Assertions.assertThat(studio)
                .isNull();
    }

    @Test
    public void create(){

        Studio studio = new Studio();
        studio.setName("Fox Wolrd");

        studioService.save(studio);

        Studio studio1 = studioService.findByName(studio.getName());

        Assertions.assertThat(studio1)
                .isNotNull();

        studioService.delete(studio1);
    }

    @Test
    public void update(){

        Studio studio = new Studio();
        studio.setName("ESPN STUDIO");

        studioService.save(studio);

        Studio studio1 = studioService.findByName(studio.getName());

        studio1.setName("FOX STUDIO");

        studio = studioService.save(studio1);

        Assertions.assertThat(studio.getName())
                .isEqualTo(studio1.getName());

        studioService.delete(studio);
    }

    @Test
    public void delete(){
        Studio studio = new Studio();
        studio.setName("John Rufos");

        studio = studioService.save(studio);

        studioService.delete(studio);

        studio = studioService.findByName("Merlin Smith");

        Assertions.assertThat(studio)
                .isNull();

    }

}
