package com.api.texo.inicializador;

import com.api.texo.domain.entity.Movie;
import com.api.texo.domain.entity.Producer;
import com.api.texo.domain.entity.Studio;
import com.api.texo.domain.exception.GenericErrorException;
import com.api.texo.domain.service.MovieService;
import com.api.texo.domain.service.ProducerService;
import com.api.texo.domain.service.StudioService;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
 class Inicializador {

    @Autowired
    ProducerService producerService;

    @Autowired
    StudioService studioService;

    @Autowired
    MovieService movieService;

    @DependsOn("liquibase")
    @PostConstruct
    public void initiator() {

            Movie movie;

            for (String[] infos : readCSV()) {

                  movie = new Movie();
                  movie.setYear( Integer.parseInt(infos[0]));
                  movie.setTitle(infos[1].trim());
                  movie.setStudios(searchStudio(infos[2].trim()));
                  movie.setProducers(searchProducer(infos[3].trim()));
                  movie.setWinner(infos[4].contains("yes"));

                  movieService.save(movie);
            }
    }

    private List<String[]> readCSV(){

        Reader reader = null;
        List<String[]> movies = new ArrayList<>();
        CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
        try {
            reader = Files.newBufferedReader(Paths.get("movielist.csv"));

            CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).withCSVParser(parser).build();

            movies = csvReader.readAll();

        } catch (IOException e) {
            throw new GenericErrorException(e.getMessage());
        }
        return movies;
    }

    private List<Producer> searchProducer(String producer){

        List<Producer> producers = new ArrayList<>();
        producer = producer.replaceAll("and",",");

        for(String name : producer.split(",")){
            Producer producer1 = producerService.findByName(name.trim());

            producers.add(Objects.isNull(producer1) ? producerService.save(new Producer(name.trim())) : producer1);
        }
        return producers;
    }

    private List<Studio> searchStudio(String studio){

        List<Studio> studios = new ArrayList<>();
        studio = studio.replaceAll("and",",");

        for(String name : studio.split(",")){
            Studio studio1 = studioService.findByName(name.trim());
            studios.add(Objects.isNull(studio1) ? studioService.save(new Studio(name.trim())) : studio1);
        }
        return studios;
    }
}
