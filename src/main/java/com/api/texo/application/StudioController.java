package com.api.texo.application;

import com.api.texo.domain.entity.Studio;
import com.api.texo.domain.service.StudioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/api/studios")
public class StudioController {

    @Autowired
    StudioService studioService;

    @GetMapping(value = "/{idStudio}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Optional<Studio>> findById(@PathVariable("idStudio") Long idStudio) {
        return ResponseEntity.ok(studioService.findById(idStudio));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> create(@RequestBody Studio studio) {
        studioService.save(studio);

        final URI location = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(studio.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> update(@RequestBody Studio studio) {
        studioService.save(studio);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/{idStudio}")
    public ResponseEntity<Void> remove(@PathVariable("idStudio") Long idStudio) {
        studioService.deleteById(idStudio);
        return ResponseEntity.noContent().build();
    }
}
