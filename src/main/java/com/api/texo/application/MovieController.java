package com.api.texo.application;

import com.api.texo.domain.entity.Movie;
import com.api.texo.domain.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/api/movies")
public class MovieController {

    @Autowired
    MovieService movieService;

    @GetMapping(value = "/{idMovie}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Optional<Movie>> findById(@PathVariable("idMovie") Long idMovie) {
        return ResponseEntity.ok(movieService.findById(idMovie));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> create(@RequestBody Movie movie) {
        movieService.save(movie);

        final URI location = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(movie.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> update(@RequestBody Movie movie) {
        movieService.save(movie);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/{idMovie}")
    public ResponseEntity<Void> remove(@PathVariable("idMovie") Long idMovie) {
        movieService.deleteById(idMovie);
        return ResponseEntity.noContent().build();
    }

}