package com.api.texo.application;

import com.api.texo.domain.dto.Winners;
import com.api.texo.domain.entity.Producer;
import com.api.texo.domain.service.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/api/producers")
public class ProducerController {

    @Autowired
    ProducerService producerService;

    @GetMapping(value = "/{idProducer}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Optional<Producer>> findById(@PathVariable("idProducer") final Long idProducer) {
        return ResponseEntity.ok(producerService.findById(idProducer));
    }

    @GetMapping(value = "/winners", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Winners> findWinners() {
        return ResponseEntity.ok(producerService.searchPoducerWinners());
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> create(@RequestBody Producer producer) {
        producerService.save(producer);

        final URI location = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(producer.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> update(@RequestBody Producer producer) {
        producerService.save(producer);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/{idProducer}")
    public ResponseEntity<Void> remove(@PathVariable("idProducer") Long idProducer) {
        producerService.deleteById(idProducer);
        return ResponseEntity.noContent().build();
    }
}