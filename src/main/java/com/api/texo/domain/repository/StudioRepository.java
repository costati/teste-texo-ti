package com.api.texo.domain.repository;

import com.api.texo.domain.entity.Studio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StudioRepository extends JpaRepository<Studio, Long> {
    Studio findByName(@Param("name") String name);
}
