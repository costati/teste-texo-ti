package com.api.texo.domain.repository;

import com.api.texo.domain.entity.Producer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProducerRepository extends JpaRepository<Producer, Long> {

    Producer findByName(@Param("name") String name);
}

