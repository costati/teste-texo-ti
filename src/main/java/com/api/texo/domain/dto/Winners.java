package com.api.texo.domain.dto;

import java.util.ArrayList;
import java.util.List;

public class Winners {

    public Winners(){
        this.min = new ArrayList<>();
        this.max = new ArrayList<>();
    };

    List<Winner> min;

    List<Winner> max;

    public List<Winner> getMin() {
        return min;
    }

    public void setMin(List<Winner> min) {
        this.min = min;
    }

    public List<Winner> getMax() {
        return max;
    }

    public void setMax(List<Winner> max) {
        this.max = max;
    }

    public static class Winner{
        String producer;
        Integer internal;
        Integer previousWin;
        Integer followingWin;

        public Winner(String producer, Integer internal, Integer previousWin, Integer followingWin) {
            this.producer = producer;
            this.internal = internal;
            this.previousWin = previousWin;
            this.followingWin = followingWin;
        }

        public Winner(){}

        public String getProducer() {
            return producer;
        }

        public void setProducer(String producer) {
            this.producer = producer;
        }

        public Integer getInternal() {
            return internal;
        }

        public void setInternal(Integer internal) {
            this.internal = internal;
        }

        public Integer getPreviousWin() {
            return previousWin;
        }

        public void setPreviousWin(Integer previousWin) {
            this.previousWin = previousWin;
        }

        public Integer getFollowingWin() {
            return followingWin;
        }

        public void setFollowingWin(Integer followingWin) {
            this.followingWin = followingWin;
        }
    }

}