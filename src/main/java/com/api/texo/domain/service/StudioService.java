package com.api.texo.domain.service;

import com.api.texo.domain.entity.Studio;
import com.api.texo.domain.exception.GenericErrorException;
import com.api.texo.domain.repository.StudioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Objects;

@Service
public class StudioService extends ServiceAbstract<Studio, Long> {

    @Autowired
    StudioRepository studioRepository;

    public StudioService(StudioRepository studioRepository) {
        super(studioRepository);
    }

    public Studio findByName(String name) {
        try {
            Studio studio = studioRepository.findByName(name);
//            if (Objects.isNull(studio)) {
//                return studioRepository.save(new Studio(name));
//            } else {
                return studio;
//            }
        } catch (Exception e) {
            throw new GenericErrorException(e.getMessage());
        }
    }
}