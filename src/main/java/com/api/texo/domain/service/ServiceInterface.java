package com.api.texo.domain.service;

import java.util.List;
import java.util.Optional;

public interface ServiceInterface<T,I> {

    T save(T t);
    Optional<T> findById(I id);
    List<T> findAll();
    void delete(T t);

}