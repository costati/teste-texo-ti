package com.api.texo.domain.service;


import com.api.texo.domain.dto.Winners;
import com.api.texo.domain.entity.Movie;
import com.api.texo.domain.entity.Producer;
import com.api.texo.domain.repository.ProducerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ProducerService extends ServiceAbstract<Producer, Long> {

    @Autowired
    ProducerRepository producerRepository;

    public ProducerService(ProducerRepository producerRepository) {
        super(producerRepository);
    }

    public Producer findByName(String name){
        if(name.trim().length()<1){
            return null;
        }
        Producer producer = producerRepository.findByName(name);
//        if(Objects.isNull(producer)){
//            return producerRepository.save(new Producer(name));
//        }else {
            return producer;
//        }
    }

    public Winners searchPoducerWinners(){

        Winners winners = new Winners();
        List<Producer> producers = producerRepository.findAll();

        List<Winners.Winner> minWinner = new ArrayList<>();
        List<Winners.Winner> maxWinner = new ArrayList<>();
        Winners.Winner winner;

        Integer anoIni = 0;
        Integer anoFin = 0;
        int minAnos = 100;
        int maxAnos = 0;
        int indice = 0;
        for(Producer producer : producers)
        {
            List<Movie> moviesWin = producer.getMovies()
                    .stream()
                    .filter(Movie::isWinner)
                    .collect(Collectors.toList());

            if(moviesWin.size()>1) {
                while(indice < moviesWin.size()) {

                    anoIni =  moviesWin.get(indice).getYear();
                    anoFin = (indice + 1) < moviesWin.size() ?  moviesWin.get(indice + 1).getYear() : 0;

                    int totAnos = anoFin - anoIni;

                    if(anoFin > 0) {
                        if (totAnos <= minAnos) {
                            winner = new Winners.Winner(
                                    producer.getName(),
                                    totAnos,
                                    anoIni,
                                    anoFin
                            );

                            if(totAnos < minAnos){
                                minWinner = new ArrayList<>();
                            }
                            minWinner.add(winner);
                            minAnos = totAnos;
                        }
                        if (totAnos >= maxAnos) {
                            winner = new Winners.Winner(
                                    producer.getName(),
                                    totAnos,
                                    anoIni,
                                    anoFin
                            );

                            if(totAnos > maxAnos){
                                maxWinner = new ArrayList<>();
                            }

                            maxWinner.add(winner);
                            maxAnos = totAnos;
                        }
                    }
                    indice++;
                }
                indice = 0;
            }
        }
        winners.setMax(maxWinner);
        winners.setMin(minWinner);

        return winners;
    }
}
