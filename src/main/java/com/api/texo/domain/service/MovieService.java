package com.api.texo.domain.service;

import com.api.texo.domain.entity.Movie;
import com.api.texo.domain.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovieService extends ServiceAbstract<Movie, Long> {

    @Autowired
    MovieRepository movieRepository;

    public void deleteAll(){
        movieRepository.deleteAll();
    }

    public MovieService(MovieRepository movieRepository) {
        super(movieRepository);
        this.movieRepository = movieRepository;
    }
}