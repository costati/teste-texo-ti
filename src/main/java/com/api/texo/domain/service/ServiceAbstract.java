package com.api.texo.domain.service;

import com.api.texo.domain.exception.EntityNotFoundException;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

abstract class ServiceAbstract<T,I> implements ServiceInterface<T,I>{

    JpaRepository<T,I> interfaceRepository;

    public  ServiceAbstract( JpaRepository<T,I> interfaceRepository){
        this.interfaceRepository = interfaceRepository;
    }

    @Override
    public T save(T t) {
        return interfaceRepository.save(t);
    }

    @Override
    public Optional<T> findById(I id) {
        return interfaceRepository.findById(id);
    }

    @Override
    public List<T> findAll() {
        return interfaceRepository.findAll();
    }

    @Override
    public void delete(T t) {
        interfaceRepository.delete(t);;
    }

    public void deleteById(I id){
        interfaceRepository.deleteById(id);
    }
}